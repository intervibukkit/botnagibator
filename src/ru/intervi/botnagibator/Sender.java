package ru.intervi.botnagibator;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.Bukkit;

import java.util.Timer;
import java.util.TimerTask;
import java.util.ArrayList;

public class Sender {
	private Main main;
	private Timer timer = new Timer();
	
	Sender (Main m) {
		main = m;
		startTimer();
	}
	
	private class Data {
		Player player;
		long time;
		int hashname;
	}
	
	private ArrayList<Data> list = new ArrayList<Data>();
	
	public void startTimer() { //старт таймера
		this.timer = new Timer();
		this.timer.schedule(new Send(), main.config.sendmills, main.config.sendmills);
	}
	
	public void stopTimer() { //стоп таймера
		try {
			this.timer.cancel();
		} catch (Exception e) {e.printStackTrace();}
	}
	
	public void add(Player player) { //добавить игрока на обработку
		Data data = new Data();
		data.player = player;
		data.time = System.currentTimeMillis();
		data.hashname = player.getName().hashCode();
		list.add(data);
	}
	
	public void remove(Player player) { //удалить игрока из обработки
		int hashname = player.getName().hashCode();
		for (int i = 0; i < list.size(); i++) {
			if (hashname == list.get(i).hashname) {
				list.remove(i);
				break;
			}
		}
	}
	
	private class Send extends TimerTask {
		@Override
		public void run() {
			long time = System.currentTimeMillis();
			for (int i = 0; i < list.size(); i++) {
				try {
					Data data = list.get(i);
					if (!data.player.isOnline()) { //выкидываем оффлайн игроков
						list.remove(i);
						continue;
					}
					if (((time / 1000) - (data.time / 1000)) > main.config.timeaction) { //если время вышло
						Kick kick = new Kick(data.player);
						kick.runTask(Bukkit.getPluginManager().getPlugin("BotNagibator"));
						list.remove(i);
					} else { //отправка уведомлений
						String text[] = new String[main.config.alert.length];
						for (int n = 0; n < text.length; n++) {
							text[n] = ChatColor.translateAlternateColorCodes('&', main.config.alert[n].replaceAll("%sec%", String.valueOf(main.config.timeaction - ((time / 1000) - (data.time / 1000)))));
						}
						data.player.sendMessage(text);
					}
				} catch (Exception e) {e.printStackTrace();}
			}
		}
	}
	
	private class Kick extends BukkitRunnable { //класс для кика игрока
		private Player player;
		Kick (Player p) {
			player = p;
		}
		
		@Override
		public void run() {
			player.kickPlayer(main.config.kick);
		}
	}
}