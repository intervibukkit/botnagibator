package ru.intervi.botnagibator;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SQL {
	private Main main;
	
	private static Connection con;
	private static Statement stmt;
	private static ResultSet rs;
	
	SQL (Main m) {
		main = m;
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (Exception e) {e.printStackTrace();}
		if (!main.config.firststart) connect();
	}
	
	public void connect() { //подключить БД
		String url = "jdbc:mysql://" + main.config.host + ":" + main.config.port + "/" + main.config.base;
		String query = "select count(*) from " + main.config.table;
		try {
			con = DriverManager.getConnection(url, main.config.user, main.config.pass);
			stmt = con.createStatement();
			rs = stmt.executeQuery(query);
			try {
				while (rs.next()) {
					int count = 0; count = rs.getInt(1);
					System.out.println("[BotNagibator] Строк в таблице: " + String.valueOf(count));
				}
			} catch (Exception e) {e.printStackTrace();}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {con.close();} catch(SQLException e) {}
            try {stmt.close();} catch(SQLException e) {}
            try {rs.close();} catch(SQLException e) {}
		}
	}
	
	public void tp(String name) { //отправить ник на телепортацию
		String url = "jdbc:mysql://" + main.config.host + ":" + main.config.port + "/" + main.config.base;
		String query = "INSERT INTO " + main.config.table + " VALUES ('" + name + "');";
		try {
			con = DriverManager.getConnection(url, main.config.user, main.config.pass);
			stmt = con.createStatement();
			try {
				stmt.executeUpdate(query);
			} catch (Exception e) {e.printStackTrace();}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {con.close();} catch(SQLException e) {}
            try {stmt.close();} catch(SQLException e) {}
            try {rs.close();} catch(SQLException e) {}
		}
	}
}