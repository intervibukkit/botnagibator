package ru.intervi.botnagibator;

import java.io.File;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Iterator;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.entity.Player;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;

import ru.intervi.botnagibator.file.Config;
import ru.intervi.botnagibator.file.Spawn;
import ru.intervi.botnagibator.file.ConfigWriter;

public class Main extends JavaPlugin implements Listener {
	public Config config = new Config();
	public Spawn spawn = new Spawn();
	private Events events = new Events(this);
	private Timer timer = null;
	
	@Override
	public void onEnable() {
		getServer().getPluginManager().registerEvents(this, this);
		getServer().getPluginManager().registerEvents(events, this);
		spawn.load();
		if (config.gc) {
			timer = new Timer();
			timer.schedule(new Gc(), (config.gcint * 60 * 1000), (config.gcint * 60 * 1000));
		}
		System.out.println("[BotNagibator] ---> Created by InterVi <---");
	}
	
	@Override
	public void onDisable() {
		events.sender.stopTimer();
		if (timer != null) {
			try {
				timer.cancel();
				timer = null;
			} catch (Exception e) {e.printStackTrace();}
		}
	}
	
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		boolean result = false;
		if (command.getName().equalsIgnoreCase("bnb")) {
			if (args != null) {
				if (args.length > 0) {
					if (args[0].equalsIgnoreCase("reload")) {
						if (sender.isOp()) {
							events.sender.stopTimer();
							config.load();
							spawn.load();
							events.sql.connect();
							events.sender.startTimer();
							if (timer != null) {
								try {
									timer.cancel();
									timer = null;
								} catch (Exception e) {e.printStackTrace();}
							}
							timer = new Timer();
							timer.schedule(new Gc(), (config.gcint * 60 * 1000), (config.gcint * 60 * 1000));
							sender.sendMessage("конфиг перезагружен");
							result = true;
						} else sender.sendMessage("нет прав");
					}
					if (args[0].equalsIgnoreCase("setspawn")) {
						if (sender instanceof Player) {
							if (sender.isOp()) {
								Location loc = ((Player) sender).getLocation();
								String world = loc.getWorld().getName();
								String x = String.valueOf(loc.getX());
								String y = String.valueOf(loc.getY());
								String z = String.valueOf(loc.getZ());
								String yaw = String.valueOf(loc.getYaw());
								String pitch = String.valueOf(loc.getPitch());
								String sep = File.separator;
								String patch = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
								patch = patch.substring(0, (patch.indexOf((sep + "plugins" + sep))));
								File folder = new File((patch + sep + "plugins" + sep + "BotNagibator"));
								if (!folder.isDirectory()) folder.mkdir();
								String sp = ((patch + sep + "plugins" + sep + "BotNagibator" + sep + "spawn.yml"));
								ConfigWriter writer = new ConfigWriter();
								writer.setConfig(sp);
								writer.setOption("world", world);
								writer.setOption("x", x);
								writer.setOption("y", y);
								writer.setOption("z", z);
								writer.setOption("yaw", yaw);
								writer.setOption("pitch", pitch);
								spawn.load();
								sender.sendMessage("спавн установлен");
								result = true;
								
							} else sender.sendMessage("нет прав");
						} else sender.sendMessage("эту команду можно выполнять только из игры");
					}
					if (args[0].equalsIgnoreCase("kill")) {
						if (sender instanceof Player) {
							if (sender.isOp()) {
								int k = 0;
								Iterator<Entity> iter = ((Player) sender).getLocation().getWorld().getEntities().iterator();
								while (iter.hasNext()) {
									Entity e = iter.next();
									if (!e.getType().equals(EntityType.PLAYER)) {
										e.remove();
										k++;
									}
								}
								sender.sendMessage("Убито мобов: " + String.valueOf(k));
							} else sender.sendMessage("нет прав");
						} else sender.sendMessage("эту команду можно выполнять только из игры");
					}
				} else {
					if (sender.isOp()) {
						sender.sendMessage("-----< BotNagibator >-----");
						sender.sendMessage("/bnb reload - перезагрузить конфиг");
						sender.sendMessage("/bnb setspawn - установить спавн");
						sender.sendMessage("/bnb kill - убить всех мобов в этом мире");
						sender.sendMessage("-----< Created by InterVi >-----");
					} else sender.sendMessage("нет прав");
				}
			}
		}
		return result;
	}
	
	private class Gc extends TimerTask { //класс для вызова сборки мусора
		@Override
		public void run() {
			try {
				System.gc();
			} catch (Exception e) {e.printStackTrace();}
		}
	}
}