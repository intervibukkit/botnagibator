package ru.intervi.botnagibator.file;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

import org.bukkit.ChatColor;

public class Config {
	public Config() {
		load();
	}
	
	//настройка подключения MySQL
	public String host = "127.0.0.1"; //хост
	public String port = "3306"; //порт
	public String base = "BotNagibator"; //имя БД
	public String table = "tp"; //имя таблицы
	public String user = "root"; //пользователь
	public String pass = "lalka"; //пароль
	
	public boolean slapblock = false; //прохождение проверки ударом по блоку
		public String block = "TNT";
	public boolean slapmob = false; //прохождение проверки ударом моба
		public String mob = "VILLAGER"; //тип моба
		public boolean immortal = true; //мобу не наносится урон
	public int timeaction = 10; //время в секундах на прохождение проверки
	public String kick = "Не успел!"; //сообщение кика
	
	public boolean noblockdamage = true; //нельзя наносить урон блокам
	public boolean nobuild = true; //нельзя ломать и ставить блоки
	public boolean noclicks = true; //нельзя ПКМ
	public boolean nodamage = true; //отменяет урон всем сущностям (при false только игрокам)
	public boolean nochat = true; //нельзя писать в чат
	public boolean nocommand = true; //нельзя использовать команды
	public boolean invincible = true; //игроки невидимы
	public boolean hidestream = true; //скрывать сообщения о входе и выходе
	public boolean noachivment = true; //отключить достижения
	
	public boolean kickfastcom = false; //кик за быстрый ввод команды
		public int comsendms = 600; //время в миллесекундах с момента захода
		public String fastreason = "Бот?"; //причина кика
	
	public boolean kickchat = true; //кикать за написание в чат команд или сообщений
		public String chatreason = "Бот?"; //причина кика
	
	public boolean kickfastcheck = false; //кик за слишком быстрое прохождение проверки
		public int checkms = 300; //время в миллесекундах с момента захода
		public String fchreason = "Бот?"; //причина кика
		
	public boolean gc = true; //включить ли принудительную сборку мусора
		public int gcint = 5; //интервал в минутах
	
	//уведомление в чат, %sec% - оставшееся время
	public String alert[] = {
			"--------------------------------",
			"=< СКОРЕЕ УДАРЬ ЖИТЕЛЯ! >=",
			"-< Осталось секунд: %sec% >-",
			"--------------------------------"
	};
	public int sendmills = 500; //интервал обработки списка игроков
	
	public boolean firststart = false;
	
	public void load() { //загрузка конфига
		String sep = File.separator;
		String patch = new File(this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath()).getAbsolutePath();
		patch = patch.substring(0, (patch.indexOf((sep + "plugins" + sep))));
		File folder = new File((patch + sep + "plugins" + sep + "BotNagibator"));
		if (!folder.isDirectory()) folder.mkdir();
		File conf = new File((patch + sep + "plugins" + sep + "BotNagibator" + sep + "config.yml"));
		if (!conf.isFile()) { //создание конфига по умолчанию
			firststart = true;
			try {
				FileOutputStream fos = new FileOutputStream(conf);
				InputStream stream = Config.class.getResourceAsStream("/config.yml");
				byte[] buff = new byte[65536];
				int n;
				while((n = stream.read(buff)) > 0){
					fos.write(buff, 0, n);
					fos.flush();
				}
				fos.close();
				buff = null;
			} catch(Exception e) {
				System.out.println("saveConfig " + "/config.yml" + " to " + conf.getAbsolutePath() + " Exception:");
				e.printStackTrace();
			}
		} else { //загрузка данных из файла
			ConfigLoader loader = new ConfigLoader();
			loader.load(conf);
			this.host = loader.getString("host");
			this.port = loader.getString("port");
			this.base = loader.getString("base");
			this.table = loader.getString("table");
			this.user = loader.getString("user");
			this.pass = loader.getString("pass");
			this.slapblock = loader.getBoolean("slapblock");
			this.block = loader.getString("block");
			this.slapmob = loader.getBoolean("slapmob");
			this.mob = loader.getString("mob");
			this.immortal = loader.getBoolean("immortal");
			this.timeaction = loader.getInt("timeaction");
			this.kick = ChatColor.translateAlternateColorCodes('&', loader.getString("kick"));
			this.noblockdamage = loader.getBoolean("noblockdamage");
			this.nobuild = loader.getBoolean("nobuild");
			this.noclicks = loader.getBoolean("noclicks");
			this.nodamage = loader.getBoolean("nodamage");
			this.nochat = loader.getBoolean("nochat");
			this.nocommand = loader.getBoolean("nocommand");
			this.invincible = loader.getBoolean("invincible");
			this.hidestream = loader.getBoolean("hidestream");
			this.alert = loader.getStringArray("alert");
			this.sendmills = loader.getInt("sendmills");
			this.noachivment = loader.getBoolean("noachivment");
			this.kickfastcom = loader.getBoolean("kickfastcom");
			this.comsendms = loader.getInt("comsendms");
			this.fastreason = ChatColor.translateAlternateColorCodes('&', loader.getString("fastreason"));
			this.gc = loader.getBoolean("gc");
			this.gcint = loader.getInt("gcint");
			this.kickfastcheck = loader.getBoolean("kickfastcheck");
			this.checkms = loader.getInt("checkms");
			this.fchreason = ChatColor.translateAlternateColorCodes('&', loader.getString("fchreason"));
			this.kickchat = loader.getBoolean("kickchat");
			this.chatreason = ChatColor.translateAlternateColorCodes('&', loader.getString("chatreason"));
		}
	}
}