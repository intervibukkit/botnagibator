package ru.intervi.botnagibator.file;

import org.bukkit.Location;
import org.bukkit.Bukkit;
import org.bukkit.World;

import java.io.File;

public class Spawn {
	public Location spawn = null;
	
	public void load() {
		String sep = File.separator;
		String patch = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
		patch = patch.substring(0, (patch.indexOf((sep + "plugins" + sep))));
		File folder = new File((patch + sep + "plugins" + sep + "BotNagibator"));
		if (!folder.isDirectory()) folder.mkdir();
		File sp = new File((patch + sep + "plugins" + sep + "BotNagibator" + sep + "spawn.yml"));
		if (sp.isFile()) {
			ConfigLoader loader = new ConfigLoader();
			loader.load(sp);
			World world = Bukkit.getWorld(loader.getString("world"));
			double x = loader.getDouble("x");
			double y = loader.getDouble("y");
			double z = loader.getDouble("z");
			float yaw = new Double(loader.getDouble("yaw")).floatValue();
			float pitch = new Double(loader.getDouble("pitch")).floatValue();
			this.spawn = new Location(world, x, y, z, yaw, pitch);
		}
	}
}