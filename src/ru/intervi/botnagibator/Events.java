package ru.intervi.botnagibator;

import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockDamageEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.entity.EntityType;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.entity.Player;
import org.bukkit.Bukkit;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerAchievementAwardedEvent;
import org.bukkit.entity.Entity;

import java.util.Iterator;

public class Events implements Listener {
	private Main main;
	public SQL sql;
	public Sender sender;
	
	Events(Main m) {
		main = m;
		this.sql = new SQL(m);
		this.sender = new Sender(m);
	}
	
	//хэш для предотвращения дублирования записей в БД
	private int hash[] = new int[10]; private int p = 0;
	private boolean isAdded(int h) {
		boolean result = false;
		for (int i = 0; i < hash.length; i++) {
			if (hash[i] == h) {
				result = true;
				break;
			}
		}
		return result;
	}
	private void addHash(int h) {
		if (p < hash.length) {
			hash[p] = h;
			p++;
		} else {
			p = 0;
			hash[p] = h;
			p++;
		}
	}
	private void remHash(int h) {
		for (int i = 0; i < hash.length; i++) {
			if (hash[i] == h) {
				hash[i] = 0;
				break;
			}
		}
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onBreak(BlockBreakEvent event) { //игрок сломал блок
		if (main.config.nobuild) {
			if (!event.getPlayer().isOp()) event.setCancelled(true);
		}
		if (main.config.slapblock && !main.config.firststart) {
			Player player = event.getPlayer();
			if (!player.isOp() && event.getBlock().getType().toString().equalsIgnoreCase(main.config.block) && !isAdded(player.getName().hashCode())) {
				if (main.config.kickfastcheck) { //проверка на слишком быстрое прохождение
					if ((System.currentTimeMillis() - player.getLastPlayed()) <= main.config.checkms) {
						player.kickPlayer(main.config.fchreason);
						System.out.println("[BotNagibator] " + player.getAddress().getAddress().getHostAddress() + " слишком быстро прошел проверку");
						return;
					}
				}
				sql.tp(player.getName());
				sender.remove(player);
				addHash(player.getName().hashCode());
				System.out.println("[BotNagibator] игрок " + player.getName() + " прошел проверку");
			}
		}
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onPlace(BlockPlaceEvent event) { //игрок поставил блок
		if (main.config.nobuild) {
			if (!event.getPlayer().isOp()) event.setCancelled(true);
		}
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onBlockDamage(BlockDamageEvent event) { //игрок бьет по блоку
		Player player = event.getPlayer();
		if (main.config.noblockdamage) {
			if (!player.isOp()) event.setCancelled(true);
		}
		if (main.config.slapblock && !main.config.firststart) {
			if (!player.isOp() && event.getBlock().getType().toString().equalsIgnoreCase(main.config.block) && !isAdded(player.getName().hashCode())) {
				if (main.config.kickfastcheck) { //проверка на слишком быстрое прохождение
					if ((System.currentTimeMillis() - player.getLastPlayed()) <= main.config.checkms) {
						player.kickPlayer(main.config.fchreason);
						System.out.println("[BotNagibator] " + player.getAddress().getAddress().getHostAddress() + " слишком быстро прошел проверку");
						return;
					}
				}
				sql.tp(player.getName());
				sender.remove(player);
				addHash(player.getName().hashCode());
				System.out.println("[BotNagibator] игрок " + player.getName() + " прошел проверку");
			}
		}
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onInteract(PlayerInteractEvent event) { //игрок взаимодействует
		if (main.config.noclicks) {
			Player player = event.getPlayer();
			if (!player.isOp() && event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) event.setCancelled(true);
		}
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onDamage(EntityDamageEvent event) { //сущность получила урон
		if (main.config.nodamage) event.setCancelled(true); else {
			Entity entity = event.getEntity();
			if (entity.getType().equals(EntityType.PLAYER) | main.config.immortal & entity.getType().toString().equalsIgnoreCase(main.config.mob)) event.setCancelled(true);
		}
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onDamageAtEntity(EntityDamageByEntityEvent event) { //сущность получила урон от сущности
		if (main.config.slapmob && !main.config.firststart) {
			Entity entity = event.getEntity();
			if (entity.getType().toString().equalsIgnoreCase(main.config.mob) && event.getDamager().getType().equals(EntityType.PLAYER)) {
				Player player = ((Player) event.getDamager());
				if (!player.isOp()) {
					if (!isAdded(player.getName().hashCode())) {
						if (main.config.kickfastcheck) { //проверка на слишком быстрое прохождение
							if ((System.currentTimeMillis() - player.getLastPlayed()) <= main.config.checkms) {
								player.kickPlayer(main.config.fchreason);
								System.out.println("[BotNagibator] " + player.getAddress().getAddress().getHostAddress() + " слишком быстро прошел проверку");
								return;
							}
						}
						sql.tp(player.getName());
						sender.remove(player);
						addHash(player.getName().hashCode());
						System.out.println("[BotNagibator] игрок " + player.getName() + " прошел проверку");
					}
				} else entity.remove();
			}
		}
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onChat(AsyncPlayerChatEvent event) { //игрок пишет в чат
		if (main.config.nochat) {
			if (!event.getPlayer().isOp()) event.setCancelled(true);
		}
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onCommand(PlayerCommandPreprocessEvent event) { //игрок набирает команду
		boolean c = false;
		Player player = event.getPlayer();
		if (main.config.nocommand) {
			if (!player.isOp()) {
				event.setCancelled(true);
				c = true;
			}
		}
		if (main.config.kickchat && c) {
			player.kickPlayer(main.config.chatreason);
			System.out.println("[BotNagibator] " + player.getAddress().getAddress().getHostAddress() + " написал в чат");
			return;
		}
		if (main.config.kickfastcom && c) {
			if ((System.currentTimeMillis() - player.getLastPlayed()) <= main.config.comsendms) {
				player.kickPlayer(main.config.fastreason);
				System.out.println("[BotNagibator] " + player.getAddress().getAddress().getHostAddress() + " слишком быстро написал команду");
			}
		}
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onAchivment(PlayerAchievementAwardedEvent event) {
		if (main.config.noachivment) {
			if (!event.getPlayer().isOp()) event.setCancelled(true);
		}
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onJoin(PlayerJoinEvent event) { //игрок заходит на сервер
		Player player = event.getPlayer();
		if (main.spawn.spawn != null) player.teleport(main.spawn.spawn);
		if (!player.isOp()) sender.add(player);
		if (main.config.invincible) {
			Iterator<? extends Player> iter = Bukkit.getOnlinePlayers().iterator();
			while (iter.hasNext()) {
				Player plr = iter.next();
				player.hidePlayer(plr);
				plr.hidePlayer(player);
			}
		}
		if (main.config.hidestream) event.setJoinMessage(null);
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onQuit(PlayerQuitEvent event) { //игрок выходит с сервера
		if (main.config.hidestream) event.setQuitMessage(null);
		remHash(event.getPlayer().getName().hashCode());
	}
	
}